export const fetchUserData = async (session) => {
    const response = await fetch('/api/userData', {
        method: 'POST',
        body: JSON.stringify({ sessionID: session }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    const { user } = (await response.json())
    return user
}

export const fetchExerciseList = async() => {
    const response = await fetch('/api/exercisesData',{
        method: 'GET',
        headers:{
            'Content-Type': 'application/json'
        }
    })
    const { exercises } = await response.json()
    return exercises
}

export const fetchModifyWorkouts = async(workouts, login, session) => {
    const validSession = await fetchVerifySession(session, login)
    if(validSession === true){
        const response = await fetch('/api/modifyWorkouts',{
            method: 'POST',
            body: JSON.stringify({ workouts: workouts, login: login }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        const { success } = await response.json()
        
        return success
    }else{
        return false;
    }
}

export const fetchVerifySession = async(session, login) => {
    const response = await fetch('/auth/verifySession', {
        method: 'POST',
        body: JSON.stringify({session: session, login: login}),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    const { success } = await response.json()
    return success
}

export const fetchLoginSession = async(login,password)=>{
    const response = await fetch('/auth/login',{
        method: 'POST',
        body: JSON.stringify({password: password, login: login}),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    const {success, session} = await response.json()
    return {success: success, sessionID: session}
}

export const fetchLogout = async(session)=>{
    fetch('/auth/logout',{
        method: 'POST',
        body: JSON.stringify({session: session}),
        headers:{
            'Content-Type': 'application/json'
        }
    })
}

export const fetchRegister = async(login,password)=>{
    const response = await fetch('/auth/register',{
        method: 'POST',
        body: JSON.stringify({login: login, password: password}),
        headers:{
            'Content-Type': 'application/json'
        }
    })
   const {success} = await response.json()
   return success
}


export const fetchChangePassword = async(login, oldPassword, newPassword)=>{
    const response = await fetch('/auth/changePassword',{
        method: 'POST',
        body: JSON.stringify({login: login, oldPassword: oldPassword, newPassword: newPassword}),
        headers:{
            'Content-Type': 'application/json'
        }
    })
   const {success} = await response.json()
   return success
}

export const fetchRemoveAccount = async(session, login, password) => {
    const response = await fetch('/auth/removeAcc', {
        method: 'POST',
        body: JSON.stringify({session: session, login: login, password: password}),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    const { success } = await response.json()
    return success
}
