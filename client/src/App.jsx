import React from 'react';
import './App.css';
import Header from './components/common/Header'
import { SessionProvider } from './components/context/SessionProvider'
import ContextHelper from './components/common/ContextHelper';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import LoginForm from './components/auth/LoginForm';
import Logout from './components/auth/Logout';
import RegisterForm from './components/auth/RegisterForm';
import LandingPage from './components/landingPage/LandingPage';
import Dashboard from './components/dashboard/Dashboard'
import Loading from './components/common/Loading';
import About from './components/about/About'
function App() {
  return (
    <SessionProvider>
      <ContextHelper />
      <BrowserRouter>
        <div className="App">
          <div className="backgroundWrap"></div>
          <Header />
          <Loading>
            <content className='mainContent'>
              <Switch>
                <Route path='/login'>
                  <LoginForm />
                </Route>
                <Route path='/logout'>
                  <Logout />
                </Route>
                <Route path='/register'>
                  <RegisterForm />
                </Route>
                <Route path='/dashboard' component={Dashboard}>
                </Route>
                <Route path='/about'>
                  <About />
                </Route>
                <Route exact path='/'>
                  <LandingPage />
                </Route>
                <Route>
                  <h1>Page not found</h1>
                  <p>
                    We're sorry, we couldn't find the page you requested.
              </p>
                </Route>
              </Switch>
            </content>
          </Loading>
        </div>
      </BrowserRouter>
    </SessionProvider>
  );
}

export default App;
