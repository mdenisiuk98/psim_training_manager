import React from 'react'
import { useSessionState } from '../context/SessionProvider'
import './workoutSummary.scss'

const WorkoutSummary = (props) => {

    const {state: { user: { workouts }, exercises }} = useSessionState();
    const { match } = props;
    const workoutID = match.params.id;
    const userExercisesArray = workouts[workoutID].exercises;

    let exercisesMap = {}
    exercises.forEach((el) => {
        exercisesMap[el._id] = el
    })
    let workoutSummaryMuscleSums = {}
    let fullStrain = 0
    let estimatedTime = 0
    userExercisesArray.forEach((el)=>{
        if(exercisesMap[el.exercise].name!=="Rest"){
            exercisesMap[el.exercise].muscles.forEach((muscle)=>{
                if(workoutSummaryMuscleSums[muscle]){
                    workoutSummaryMuscleSums[muscle] += el.reps * el.weight
                }else{
                    workoutSummaryMuscleSums[muscle] = el.reps * el.weight
                }
                fullStrain += el.reps * el.weight
            })
        }
        estimatedTime+=el.reps*30

    })
    let workoutSummary = ""
    let uniqueMuscles =  Object.keys(workoutSummaryMuscleSums)
    uniqueMuscles.forEach((key, index)=>{
        workoutSummary+=(`${key} (${(workoutSummaryMuscleSums[key]/fullStrain*100).toPrecision(2)}%)${index<uniqueMuscles.length-1?", ":""}`)
    })


    return (
        <div className="summaryContainer">
            <div className="summary">
                <h1>Summary</h1>
                <h2>Muscles worked out:</h2>
                <div className="musclesWorkedOut">
                   {workoutSummary}
                </div>
                <h2>Estimated time:</h2>
                <div className="musclesWorkedOut">
                   {(estimatedTime/60).toPrecision(2)} min
                </div>
            </div>
            
            <div className="exerciseContainer">
                <h1>Exercises</h1>    
                    {userExercisesArray.map((element,index)=>
                    <p key={index} className="exercise">
                            <label className="exerciseName">{exercisesMap[element.exercise].name}</label>
                            <label className="repsWeightName">{element.reps} reps
                             {", "}{element.weight}kg weight</label>
                        </p>
                    )}
            </div>
        </div>
    )
}

export default WorkoutSummary;
