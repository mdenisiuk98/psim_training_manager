import React, { createContext, useReducer, useContext } from "react";

const initialStateValues = {
    sessionID: localStorage.getItem('sessionID') || '',
    user: {},
    exercises: [],
    loadedInitialSession: false,
    loadedInitialUser: false,
}
const SessionContext = createContext(initialStateValues)

const sessionReducer = (state, action) => {


    switch (action.type) {
        case 'newSession':
            return {
                ...state,
                sessionID: action.newSessionID,
            }
        case 'getExercises':
            return {
                ...state,
                exercises: action.newExercises
            }
        case 'newUser':
            return {
                ...state,
                user: action.newUser
            }
        case 'endSession':
            return {
                ...state,
                user: {},
                sessionID: ''
            }
        case 'addWorkout':
            return {
                ...state,
                user: {
                    ...state.user,
                    workouts: [...state.user.workouts, action.newWorkout]
                }
            }
        case 'editWorkout':
            const workoutsCopy = state.user.workouts
            workoutsCopy[action.editedID] = action.editedWorkout
            return {
                ...state,
                user: {
                    ...state.user,
                    workouts:workoutsCopy 
                }
            }
        case 'deleteWorkout':
            return {
                ...state,
                user: {
                    ...state.user,
                    workouts: state.user.workouts.filter((x, index) => index !== action.idToRemove)
                }
            }
        case 'loadInitialSession':
            return {
                ...state,
                loadedInitialSession: true
            }
        case 'loadInitialUser':
            return {
                ...state,
                loadedInitialUser: true
            }
        default:
            return state;
    }
}

export const SessionProvider = ({ children }) => {
    const [state, dispatch] = useReducer(sessionReducer, initialStateValues)
    return (
        <SessionContext.Provider value={{ state, dispatch }}>
            {children}
        </SessionContext.Provider>
    )
}

export const useSessionState = () => useContext(SessionContext)