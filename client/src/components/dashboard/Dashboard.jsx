import React from 'react'
import { Switch, Route, Link, Redirect } from 'react-router-dom'
import { useSessionState } from '../context/SessionProvider'
import './Dashboard.css'
import WorkoutsList from '../workoutsList/WorkoutsList'
import AddWorkout from '../addWorkout/AddWorkout'
import EditWorkout from '../addWorkout/EditWorkout'
import WorkoutSummary from '../workoutSummary/workoutSummary'
import ChangePassword from '../changePassword/changePassword'
import RemoveAccount from '../removeAccount/RemoveAccount'
const Dashboard = (props) => {

    const { match, location: { pathname } } = props
    const { state: { user: { login } } } = useSessionState()
    const myWorkoutsClass = `dashboardLink ${pathname.includes("/myWorkouts") || pathname.includes("/editWorkout") || pathname.includes("/addWorkout") || pathname.includes("/workoutSummary") ? " activeDashboardLink" : ""}`
    const changePassClass = `dashboardLink ${pathname.includes("/changePass") ? " activeDashboardLink" : ""}`
    const removeAccClass = `dashboardLink ${pathname.includes("/removeAcc") ? " activeDashboardLink" : ""}`

    return (
        <>
            {login
                ? <div className="dashboardContainer">
                    <div className="dashboardHeader">
                        {login ? `Welcome back, ${login}!` : ""}
                    </div>
                    <nav className="dashboardNav">
                        <ul className="dashboardLinksContainer">
                            <Link to={`${match.url}/myWorkouts`}><li className={myWorkoutsClass}>My workouts</li></Link>
                            <Link to={`${match.url}/changePass`}><li className={changePassClass}>Change password</li></Link>
                            <Link to={`${match.url}/removeAcc`}><li className={removeAccClass}>Remove account</li></Link>
                        </ul>
                    </nav>
                    <Switch>
                        <Route exact path={`${match.path}/myWorkouts`} component={WorkoutsList} />

                        <Route exact path={`${match.path}/editWorkout/:id`} component={EditWorkout} />

                        <Route exact path={`${match.path}/addWorkout/:newName`} component={AddWorkout} />

                        <Route exact path={`${match.path}/workoutSummary/:id`} component={WorkoutSummary} />

                        <Route exact path={`${match.path}/changePass`} component={ChangePassword} />
                        
                        <Route exact path={`${match.path}/removeAcc`} component={RemoveAccount} />
                      
                    </Switch>
                </div>
                : <Redirect to='/login' />
            }
        </>
    )
}

export default Dashboard
