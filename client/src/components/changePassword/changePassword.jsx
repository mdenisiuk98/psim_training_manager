import React, { useState } from 'react'
import { useSessionState } from '../context/SessionProvider'
import { fetchChangePassword } from '../../helpers/fetchData'
import './changePassword.scss'

const ChangePassword = () =>{

    
    const { state } = useSessionState()
    const [succes, setsucces] = useState(false);

    const [currentPass, setcurrentPass] = useState('');
    const [newPass, setnewPass] = useState('')
    const [confirmNewPass, setconfirmNewPass] = useState('')

    const [showCurrent, setshowCurrent] = useState(false);
    const [showPassword, setshowPassword] = useState(false);
    const [showConfirmPassword, setshowConfirmPassword] = useState(false)

    const [emptyInput, setemptyInput] = useState(false)
    const [differentPass, setdifferentPass] = useState(false)
    const [wasResetPressed, setwasResetPressed] = useState(false)


    const showCurrentPasswordFunction = () => {
        setshowCurrent(!showCurrent);
    }

    const showPasswordFunction = () =>{
        setshowPassword(!showPassword);
    }

    const showConfirmPasswordFunction = () =>{
        setshowConfirmPassword(!showConfirmPassword);
    }

    const checkPasswords = async () =>{
        console.log(`newPass ${newPass}, cur ${currentPass}, conf ${confirmNewPass}`)
        resetValues();
        setwasResetPressed(false);
        

        if(newPass === '' || currentPass === '' || confirmNewPass===''){
            setemptyInput(true);
            setwasResetPressed(true);
            
        }else if(newPass !== confirmNewPass){
            setdifferentPass(true);
            setwasResetPressed(true);
        }else{
            const response = await fetchChangePassword(state.user.login, currentPass, newPass);
            
            setsucces(response);
            setwasResetPressed(true);
        }

        console.log(`empty${emptyInput}, diff${differentPass}, suc${succes}`)
        
    }

    const resetValues = () =>{
        setdifferentPass(false);
        setemptyInput(false);
    }

    return (
        <div className="changePasswordContainer">

            <h2>Current Password</h2>
                <div className="buttonsContainer">
                    {showCurrent ? <input type="text" value={currentPass} onChange={e => setcurrentPass(e.target.value)} /> : <input type="password" value={currentPass} onChange={e => setcurrentPass(e.target.value)} />}
                    <button onClick={showCurrentPasswordFunction}>show</button>
                </div>

            <h2>New Password</h2>
                <div className="buttonsContainer">
                    {showPassword ? <input type="text" value={newPass} onChange={e => setnewPass(e.target.value)} /> : <input type="password" value={newPass} onChange={e => setnewPass(e.target.value)} />}
                    <button onClick={showPasswordFunction}>show</button>
                </div>

            <h2>Confirm New Password</h2>
                <div className="buttonsContainer">
                    {showConfirmPassword ? <input type="text" value={confirmNewPass} onChange={e => setconfirmNewPass(e.target.value)} /> : <input type="password" value={confirmNewPass} onChange={e => setconfirmNewPass(e.target.value)} />}
                    <button onClick={() => showConfirmPasswordFunction(succes)}>show</button>
                </div>

            
            <button onClick={checkPasswords} className="submit">Change password</button>

            
             { wasResetPressed ? (
                succes ? (<div className="passwordWasChanged"><h2>Password was changed!</h2></div>) : (
                    emptyInput ? (<div className="wrongPassword"><h2>Some of fields are empty</h2></div>) : (
                        differentPass ? (<div className="wrongPassword"><h2>Passwords are different</h2></div>) : (<div className="wrongPassword"><h2>Current Password is wrong!</h2></div>)
                    ))) : (<></>)
                }
        

        </div>
    )
}

export default ChangePassword;
