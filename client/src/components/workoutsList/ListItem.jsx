import React from 'react'
import { Link } from 'react-router-dom'
import { useSessionState } from '../context/SessionProvider';
import './WorkoutsList.scss'
function ListItem(props){

    const {dispatch} = useSessionState();


    const removeWorkout = () =>{
        dispatch({type: "deleteWorkout", idToRemove: props.id});
    }

    return(
        <>
            <label className="workoutName">{props.name}</label>

            <div className="buttons">
                <Link to={`/dashboard/workoutSummary/${props.id}`}><button id="Show">show</button></Link>
                <Link to={`/dashboard/editWorkout/${props.id}`}><button id="Edit">edit</button></Link>
                <button onClick={removeWorkout} id="Delete">delete</button>
            </div>
        </>
    )
}

export default ListItem;