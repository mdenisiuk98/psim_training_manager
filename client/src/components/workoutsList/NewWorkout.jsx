import React, {useState} from 'react'
import { useHistory } from 'react-router-dom'
import "./WorkoutsList.scss"

const NewWorkout = () => {

    const [inputText, setInputText] = useState('New workout');
    const history = useHistory();
    const newWorkoutHandler = () => {
        if(inputText){
            history.push(`/dashboard/addWorkout/${inputText}`);
        }else{
            window.alert('Can not be empty!');
        }
    }

    return(
        <div className="newWorkout">
           <input type="text" value={inputText} onChange={e => setInputText(e.target.value)} />
           <button onClick={newWorkoutHandler}>create new</button>
        </div>
    )
}

export default NewWorkout;