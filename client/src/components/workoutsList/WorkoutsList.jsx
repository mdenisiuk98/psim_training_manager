import React from 'react'
import ListItem from './ListItem'
import { useSessionState } from '../context/SessionProvider'
import NewWorkout from './NewWorkout';
import './WorkoutsList.scss';


function WorkoutsList(){

    const { state: { user: { workouts } } } = useSessionState()

    const workoutElement = workouts.map((name, index) =>
        <li key={index} className="list">
            <ListItem name={name.name} id={index}/>
        </li>
    );
    
    return(
       <div className="container">
            <h1 className="title">Your workouts</h1>
            <div>
                <NewWorkout />
                <ul>
                    {workoutElement}
                </ul>
            </div>
        </div>
    )
}

export default WorkoutsList;