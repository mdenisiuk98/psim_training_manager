import React, { useState, useEffect } from 'react'
import { useSessionState } from '../context/SessionProvider'
import { fetchVerifySession, fetchLogout } from '../../helpers/fetchData'
import { Redirect } from 'react-router-dom'

const Logout = () => {
    
    const { dispatch, state:{sessionID,user: {login}} }  = useSessionState()
    const [doneLoggingOut, setdoneLoggingOut] = useState(false)
    const logoutHandler = async()=>{
        if(login && sessionID){
            const validSession = await fetchVerifySession(sessionID,login)
            if(validSession){
                fetchLogout(sessionID)               
            }
        }
        setdoneLoggingOut(true)
        dispatch({type:'endSession'})
    }
    useEffect(()=>{
        logoutHandler()
    },[sessionID,login])
    
    
    return (
        <>
            {
                doneLoggingOut
                ?<Redirect to='/'/>
                :<div>
                    You're being logged out now.
                </div>
            }
        </>
    )
}

export default Logout
