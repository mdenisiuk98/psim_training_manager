import React from 'react'

const AuthInput = ({stateValue, stateHandler, name}) => {

    const handleChange = (e)=>{
        if(stateValue !== e.target.value){
            stateHandler(e.target.value)
        }
    }

    const placeholderMsg = name.toLowerCase().includes('confirm')?`Confirm your password`:`Enter your ${name}`

    return (
        <input type={name.toLowerCase().includes("password")?"password":"text"} name={name} className="authFormInput" value={stateValue} onChange={handleChange} placeholder={placeholderMsg}/>
    )
}

export default AuthInput
