import React, { useState } from 'react'
import {Redirect, Link} from 'react-router-dom'
import './Auth.css'
import { useSessionState } from '../context/SessionProvider'
import AuthInput from './AuthInput'
import { fetchRegister } from '../../helpers/fetchData'

const RegisterForm = () => {
    const { state } = useSessionState()
    const [login, setlogin] = useState('')
    const [password, setpassword] = useState('')
    const [confirmPassword, setconfirmPassword] = useState('')
    const [errorMsgLogin, seterrorMsgLogin] = useState('')
    const [errorMsgAuth, seterrorMsgAuth] = useState('')
    const [errorMsgPass, seterrorMsgPass] = useState('')
    const [errorMsgPassMatch, seterrorMsgPassMatch] = useState('')
    const [successRegister, setsuccessRegister] = useState(false)

    const failedLoginMsg = 'Your username was taken or the server failed to respond. Please try again!'
    const emptyLoginMsg = 'Your username cannot be empty!'
    const emptyPasswordMsg = 'Your password cannot be empty!'
    const passwordMismatchMsg = 'Confirmed password needs to be the same as the password above!'

    const validateRegister = ()=>{
        let valid = true
        if(!login){
            seterrorMsgLogin(emptyLoginMsg)
            valid = false
        }
        if(!password){
            seterrorMsgPass(emptyPasswordMsg)
            valid = false
        }
        if(password!==confirmPassword){
            seterrorMsgPassMatch(passwordMismatchMsg)
            valid = false
        }
        return valid
    }

    const registerHandler = async(e)=>{
        seterrorMsgLogin('')
        seterrorMsgPass('')
        seterrorMsgPassMatch('')
        e.preventDefault()
        if(validateRegister()){
            const success = await fetchRegister(login,password)
            if(success){
                setsuccessRegister(true)
            }else{
                seterrorMsgAuth(failedLoginMsg)
            }
        }
    }

    return (
        <>
        {(Object.keys(state.user).length!==0)?<Redirect to='/dashboard'/>:(
            successRegister?<div className="authSuccessMsg">
                <h1>Congratulations!</h1>
                <p>You successfully managed to register your account!</p>
                <p><Link to='/login'>Log in here</Link> to start managing your workouts!</p>
            </div>:
        <div className='authContainer'>
            <div className="authContainerHeader">
                Register
            </div>
            {
                errorMsgAuth?<div className="authErrorMsg">{errorMsgAuth}</div>:null
            }
            <form className='authForm'>
                <div className="authInputHeader">
                    Username
                </div>
                <AuthInput stateValue={login} stateHandler={setlogin} name="login"/>
                {
                    errorMsgLogin?<div className="authErrorMsg">{errorMsgLogin}</div>:null
                }
                <div className="authInputHeader">
                    Password
                </div>
                <AuthInput stateValue={password} stateHandler={setpassword} name="password"/>
                {
                    errorMsgPass?<div className="authErrorMsg">{errorMsgPass}</div>:null
                }
                <div className="authInputHeader">
                    Confirm password
                </div>
                <AuthInput stateValue={confirmPassword} stateHandler={setconfirmPassword} name="confirmPassword"/>
                {
                    errorMsgPassMatch?<div className="authErrorMsg">{errorMsgPassMatch}</div>:null
                }
                <button className="authSubmitButton" onClick={registerHandler}>Register</button>
            </form>
            <div className="authFooter"></div>
        </div>
        )
        }
        </>
    )
}

export default RegisterForm
