import React,{ useState } from 'react'
import AuthInput from './AuthInput'
import {Link, Redirect} from 'react-router-dom'
import {useSessionState} from '../context/SessionProvider'
import { fetchLoginSession } from '../../helpers/fetchData'
import './Auth.css'
const LoginForm = () => {
    const {state, dispatch} = useSessionState()
    const [login, setlogin] = useState('')
    const [password, setpassword] = useState('')
    const [errorMsgAuth, seterrorMsgAuth] = useState('')
    const [errorMsgLogin, seterrorMsgLogin] = useState('')
    const [errorMsgPass, seterrorMsgPass] = useState('')
    const failedLoginMsg = 'Your password or login was invalid. Please try again!'
    const emptyLoginMsg = 'Your login cannot be empty!'
    const emptyPasswordMsg = 'Your password cannot be empty!'
    const loginValidator = ()=>{
        let valid = true
        if(!login){
            seterrorMsgLogin(emptyLoginMsg)
            valid = false
        }else{
            seterrorMsgLogin('')
        }
        if(!password){
            seterrorMsgPass(emptyPasswordMsg)
            valid = false
        }else{
            seterrorMsgPass('')
        }

        return valid
    }

    const loginHandler= async (e)=>{
        e.preventDefault()
        seterrorMsgAuth('')
        if(loginValidator()){
            const {success, sessionID } = await fetchLoginSession(login,password)
            if(success){
             dispatch({type: 'newSession', newSessionID: sessionID})
            }else{
                setpassword('')
                seterrorMsgAuth(failedLoginMsg)
                seterrorMsgPass('')
                seterrorMsgLogin('')
            }
        }
    }
    return (
        <>
        {(Object.keys(state.user).length!==0)?<Redirect to='/dashboard'/>:
        <div className='authContainer'>
            <div className="authContainerHeader">
                Logging in
            </div>
            {
                errorMsgAuth?<div className="authErrorMsg">{errorMsgAuth}</div>:null
            }
            <form className='authForm'>
                <div className="authInputHeader">
                    Username
                </div>
                <AuthInput stateValue={login} stateHandler={setlogin} name="login"/>
                {
                    errorMsgLogin?<div className="authErrorMsg">{errorMsgLogin}</div>:null
                }
                <div className="authInputHeader">
                    Password
                </div>
                <AuthInput stateValue={password} stateHandler={setpassword} name="password"/>
                {
                    errorMsgPass?<div className="authErrorMsg">{errorMsgPass}</div>:null
                }
                <button className="authSubmitButton" onClick={loginHandler}>Log in</button>
            </form>
            <div className="authFooter">
                Not a member yet? <Link to='/register'>Sign up here</Link> to start using Training Manager.
            </div>
        </div>
        }
        </>
    )
   
}

export default LoginForm
