import React, {useState} from 'react'
import AuthInput from '../auth/AuthInput'
import { useSessionState } from '../context/SessionProvider'
import { fetchRemoveAccount } from '../../helpers/fetchData'
import './removeAccount.scss'

const RemoveAccount = () => {

    const {state: { user:{ login }, sessionID }, dispatch} = useSessionState();
    const [password, setpassword] = useState('');
    const [success, setsuccess] = useState(false)
    const [displayMsg, setdisplayMsg] = useState(false)

    const removeAcc = async(e) =>{
        e.preventDefault();
        const done = await fetchRemoveAccount(sessionID,login,password);
        setsuccess(done)
        setdisplayMsg(true)
        if(done){
            setTimeout( () => dispatch({type: "endSession"}),1000);
        }
    }

    
    
    return (
        <>
        {success?<div><h1>Goodbye!</h1><p>Thank you for trying our app!</p></div>:
        <div className="formWrapper">
            <label>Enter your password to remove the account</label>
            <AuthInput stateValue={password} stateHandler={setpassword}  name="password"/>
            <button className="reset" onClick={removeAcc}>Remove account</button>
        </div>}
        {(displayMsg && !success)?<div className="failedMsg">Something went wrong!</div>:null
}
        </>
    )
}

export default RemoveAccount
