import React from 'react';
import "./LandingPage.css"
import LandingPageFrame from './LandingPageFrame';

const LandingPage = () =>{

    const firstFrameLogoText = <>Looking for more <span className='emphasis'>efficient</span> workout planning?</>;
    const firstFrameText = <><span className='emphasis'>Training Manager</span> offer exactly that with easy to use <span className='emphasis'>planner</span> and a simple <span className='emphasis'>advice</span> system.</>;
    const firstFrameButtonText = "Sign up now";

    const secondFrameLogoText = <>Already a <span className='emphasis'>member</span>?</>;
    const secondFrameText = <>Welcome back! Log in to <span className='emphasis'>plan</span> new workouts or <span className='emphasis'>manage</span> existing ones.</>;
    const secondFrameButtonText = "Log in";

    

    return( 
        <>
        <h1 className="LandingHeading">Training Manager helps you organize your workout routine</h1>
        <div className="landignPageContent">
           
            <div className="websiteDescription">    
                <p>Start planning out your workouts in an intelligent way right now!</p>
                <p>Training Manager gives you access to a training scheduler designed with ease of use and user friendliness in mind.</p>
                <p>Access your workouts through a mobile app to keep track of exercises at the gym!</p>
                <div className="imgWrapper">
                    <img src='/biceps-min.png' alt="fotka"></img>
                </div>
            </div>

        <div className="framesContainer">
            <LandingPageFrame logo={firstFrameLogoText} text={firstFrameText} buttonText={firstFrameButtonText} id={1}/>
            <LandingPageFrame logo={secondFrameLogoText} text={secondFrameText} buttonText={secondFrameButtonText}/>
        </div>

        </div>
        </>
    )
}

export default LandingPage; 