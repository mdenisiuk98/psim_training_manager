import React from 'react';
import "./LandingPage.css"
import { Link } from 'react-router-dom';

const LandingPageFrame = (props) =>{
    

    return(
        <>
        {props.id === 1 ?
            <div className="landingPageFrame">
                <h2 className="logoFrame1">{props.logo}</h2>
                <p className="textFrame1">{props.text}</p>
                <Link to='/register'><button className="buttonFrame1" >{props.buttonText}</button></Link>
            </div> 
        
        :

            <div className="landingPageSecondFrame">
                <h2 className="logoFrame2">{props.logo}</h2>
                <p className="textFrame2">{props.text}</p>
                <Link to='/login'><button className="buttonFrame2">{props.buttonText}</button></Link>
            </div>
        }


        </>
        
    )
}

export default LandingPageFrame;
