import React from 'react'
import { useSessionState } from '../context/SessionProvider'
import "../../App.css"

const Loading = ({ children }) => {
    const { state: { loadedInitialUser, loadedInitialSession } } = useSessionState()
    return (

        <>
            {loadedInitialSession && loadedInitialUser
                ? children
                : <div className="loadingWrapper">
                    <div className="loadingSpinner">
                        Loading...
                    </div>
                </div>
            }
        </>
    )
}

export default Loading
