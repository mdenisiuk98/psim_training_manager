import {useEffect} from 'react'
import {  useSessionState } from '../context/SessionProvider'
import { fetchUserData, fetchExerciseList, fetchModifyWorkouts } from '../../helpers/fetchData'
const ContextHelper = () => {

    const {state,dispatch} = useSessionState()

    useEffect(()=>{
        (async()=>{
             if(state.loadedInitialSession===true){
                 localStorage.setItem('sessionID', state.sessionID)
             }else{
                 dispatch({type: 'loadInitialSession'})
             }
            dispatch({type: 'newUser', newUser: state.sessionID ? (await fetchUserData(state.sessionID)) : {}})
            if(!state.loadedInitialUser){
                dispatch({type: 'loadInitialUser'})
            }
        })()
        },[state.sessionID])
    useEffect(()=>{
        (async()=>{
            if(state.exercises.length===0){
                dispatch({type: 'getExercises', newExercises: await fetchExerciseList()})
            }
        })()
     },[state.exercises])
    useEffect(()=>{
        if(Object.keys(state.user).length>0 && state.loadedInitialUser){
            fetchModifyWorkouts(state.user.workouts,state.user.login,state.sessionID)
        }
        },[JSON.stringify(state.user.workouts)])

    return (null)
}

export default ContextHelper
