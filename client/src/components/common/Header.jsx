import React from 'react'
import "./Header.css"
import { useSessionState,  } from '../context/SessionProvider'
import { Link } from 'react-router-dom'

export default function Header() {
    const {state} = useSessionState()

    return (
        <header className="App-header">
            <img className='logoSVG' src='logo-min.svg' alt=''></img>
            {(state.sessionID && state.user) 
            ? <nav>
               {/* logged in navbar */}
               <Link to='/' className='navLink'>home</Link> {/*remove after its not needed*/}
               <Link to='/about' className='navLink'>about</Link>
               <Link to='/dashboard' className='navLink'>my account</Link>
               <Link to='/logout' className='navLink'>log out</Link>
            </nav> 
            : <nav>
                 {/* not logged in navbar */}
                 <Link to='/' className='navLink'>home</Link> {/*remove after its not needed*/}
                 <Link to='/about' className='navLink'>about</Link>
                 <Link to='/login' className='navLink'>login</Link>
                 <Link to='/register' className='navLink'>register</Link>
              
            </nav>}
        </header>
    )
}
