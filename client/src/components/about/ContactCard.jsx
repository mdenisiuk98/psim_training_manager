import React from 'react'
import './about.scss'

const ContactCard = (props) => {
    return (
        <div className="contactCard">
            <span className="contactName">{props.name}</span>

            <p>Github: <a className="info" href={`https://github.com/${props.github}`}> {props.github}</a></p>
            <p>Bitbucket:<a className="info" href={`https://bitbucket.org/${props.bitbucket}`}> {props.bitbucket}</a></p>
            <p>Linkedin:<a className="info" href={props.linkedin?`https://linkedin.com/${props.linkedin}`:''}> {props.linkedin}</a></p>
            <p>Mail:<a className="info" href={props.mail?`mailto:${props.mail}`:''}> {props.mail}</a></p>
            
        </div>
    )
}

export default ContactCard;
