import React from 'react'
import ContactCard from './ContactCard'
import './about.scss'
const About = () => {

    const MD = {
        name: "Mateusz",
        github: "mdenisiuk98",
        bitbucket: "mdenisiuk98",
        linkedin: "",
        mail: "m.denisiuk98@gmail.com"
    };

    const MR={
        name: "Michał",
        github: "HowToShake",
        bitbucket: "HowToShake",
        linkedin: "/in/michał-ryśkiewicz-b3b751194/",
        mail: "ryskiewicz.m@gmail.com"
    }
    return (
        <div>
            <h1>HELLO THERE!</h1>
            <h2>THIS IS PROJECT FOR  A UNIVERSITY CLASS</h2>
            <p>We are glad to see you in our website. We hope you enjoy this!</p>

            <div className="contactsWrapper">
                <ContactCard {...MD} />
                <ContactCard {...MR} />
            </div>
        </div>
    )
}

export default About;