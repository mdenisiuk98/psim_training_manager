import React from 'react'
import {useSessionState} from '../context/SessionProvider'
import { useHistory } from 'react-router-dom'
import WorokoutEditor from './WorokoutEditor'

const EditWorkout = (props) => {
    const {state, dispatch} = useSessionState();
    const { match } = props;
    const history = useHistory();

    const newWorkout = state.user.workouts[match.params.id];

    const save = (edited) =>{
        dispatch({type:"editWorkout", editedWorkout:edited, editedID: match.params.id})
        history.push('/dashboard/myWorkouts');
    }


    return(
       <div>
            <WorokoutEditor inital={newWorkout} saveHandler={save}></WorokoutEditor>
       </div>
    )
}

export default EditWorkout;