import React, {useState} from 'react'

import {useSessionState} from '../context/SessionProvider'
import { useHistory } from 'react-router-dom'
import WorokoutEditor from './WorokoutEditor'

const AddWorkout = (props) => {
    const { dispatch } = useSessionState();
    const { match } = props;
    const history = useHistory();

    const [ workout ] = useState({name: match.params.newName, exercises: [], created: Date.now()});

    const save = (newWorkout) =>{

        dispatch({type: 'addWorkout', newWorkout: newWorkout })
        history.push('/dashboard/myWorkouts');
    }

    return(
        <div>
            <WorokoutEditor inital={workout} saveHandler={save}></WorokoutEditor>
       </div>
    )
}

export default AddWorkout;