import React,{useState} from 'react'
import { useSessionState } from '../context/SessionProvider';
import './workoutEditor.scss'

const AddExercise = ({addHandler}) => {

  
    const { state: { exercises } } = useSessionState();

    const [reps, setReps] = useState(1);
    const [weights, setWeights] = useState(1);
    const [id, setid] = useState(exercises[0]._id)

    const add = ()=>{
        addHandler({exercise: id,reps: reps, weight: weights})
    }
    

    return(
        <div className="addExerciseSection">

                <h2>Add exercise</h2>
                <select onInput={e=> setid(e.target.value)} >
                    {exercises.map((element) => (
                        <option value={element._id} key={element._id}>{element.name}</option>
                    ))}
                </select>

                <div className="exerciseRepsWeight">
                    <div className="repsWeight">
                        <label>Reps</label>
                            <input type="number" value={reps} onChange={e => setReps(e.target.value)} />
                    </div>
                    
                    <div className="repsWeight">
                        <label>Weights(kg)</label>
                            <input type="number" value={weights} onChange={e => setWeights(e.target.value)} />
                    </div>
                </div>

                <button onClick={add}>add</button>

        </div>
    )
}

export default AddExercise;