import React,{useState} from 'react'
import AddedExercisesList from './AddedExercisesList'
import AddExercise from './AddExercise'
import {useHistory} from 'react-router-dom'
import './workoutEditor.scss'

const WorokoutEditor = ({inital, saveHandler}) => {

    const history = useHistory();

    const [workout, setworkout] = useState(inital)
    const [inputText, setInputText] = useState(inital.name);


    const addExercise = (newExercise)=>{
        setworkout({
            ...workout,
            exercises: [
                ...workout.exercises,
                newExercise
            ]
        })
    }


    const removeExercise = (id) =>{
       setworkout({
           ...workout,
           exercises: workout.exercises.filter((x, index) => index !== id)
       })
    }


    const save = ()=>{
        const test = {...workout, name: inputText}
        saveHandler(test)
    }


    const cancel = () =>{
        history.push('/dashboard/myWorkouts');
    }


    return (
        <div className="addWorkoutContainer">

            <div className="saveCancelNavBar">
               <input type="text" value={inputText} onChange={e => setInputText(e.target.value)} />
                <div className="buttonsContainer">
                    <button onClick={save} id="Save">save</button>
                    <button onClick={cancel} id="Cancel">cancel</button>
                </div>
            </div>
        
            <div className="editWorkoutContent">
                <AddedExercisesList workout={workout}  remove={removeExercise}/>
                <AddExercise addHandler={addExercise}/>
            </div>

        </div>
    )
}

export default WorokoutEditor
