import React from 'react'
import { useSessionState } from '../context/SessionProvider'
import './workoutEditor.scss'


const AddedExercisesList = ({workout, remove}) => {

   const {state} = useSessionState()

   const deleteExercise = (id) => {
       remove(id);
    }


   const exerciseNames = {}
   state.exercises.forEach((element)=>{
       exerciseNames[element._id] = element.name
   })

    const exerciseElements = workout.exercises.map((element,index) => 
        <div className="exerciseContainer" key={index}>
            <li>{exerciseNames[element.exercise]}</li>
            <button onClick={ () => deleteExercise(index)}>delete</button>
            <li>
                <label >{workout.exercises[index].reps} reps, {' '}
                {workout.exercises[index].weight}kg weight</label>
            </li>
        </div>
   
    );

    return(
        <div className="exerciseList">
           {exerciseElements}
        </div>
    )
}

export default AddedExercisesList;