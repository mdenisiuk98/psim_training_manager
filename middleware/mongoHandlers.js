const mongoose = require('mongoose');
const ExerciseModel = require('../models/exerciseModel')
const UserModel = require('../models/userModel')
const SessionModel = require('../models/userSessionModel')
const { exercises } = require('../config/exercises.json')
const { MONGOLOGIN, MONGOPASS, MONGOPOOLSIZE,MONGOADDRESS,
        MONGODBNAME,AUTHDB,MONGOSRV,MONGOADDITIONAL, DUMMYLOGIN, DUMMYPASS } = process.env
const connectionWrapper = async (func) =>{
    if(mongoose.connection.readyState===0){
        try{
            const mongoUri = `mongodb${MONGOSRV}://${MONGOLOGIN}:${MONGOPASS}@${MONGOADDRESS}/${MONGODBNAME}?authSource=${AUTHDB}&${MONGOADDITIONAL}`
            await mongoose.connect(mongoUri,{
                useNewUrlParser: true,
                useCreateIndex: true,
                useUnifiedTopology: true,
                poolSize: MONGOPOOLSIZE,
            });
        }catch(err){
           console.log(err)    
          // console.log(err)    
        }
    }
    let returnValue = {}
    try{
       returnValue = await func()
    }catch(err){
        console.log(err)
    }
    return returnValue
}
const mongoHandlers = {
    newUser : async(user)=>{
       return (await connectionWrapper(async ()=>{
           const userAdded = await  new UserModel(user).save()
           return userAdded
       }))
    },login : async (login,password) =>{
       const loginSession = await connectionWrapper(async()=>{
           const userLookup = await UserModel.find({login: login})
           if(userLookup.length>0){
               if(await userLookup[0].verifyPass(password)){
                  return (await new SessionModel({user: userLookup[0]._id,valid: true}).save())._id
               }else{
                return ''
               }
           }else{
            return ''
           }
       })
       return loginSession
    },logout : async (session) => {
       const success = await connectionWrapper(async()=>{
           const sessionLookup = await SessionModel.findById(session)
           if(sessionLookup){
               sessionLookup.valid=false
               await sessionLookup.save()
           }
           return sessionLookup
       })
       if(success.valid === false){
            return true
       }
       else{
           return false
       }
    },verifySession : async (session, login) => {
        const success = await connectionWrapper(async()=>{
            const sessionLookup = await SessionModel.findById(session)
            const userLookup = await UserModel.findById(sessionLookup.user)
            if(sessionLookup && userLookup.login === login){
                  return sessionLookup.valid
            }
            return false
    })
    return success
    },verifyPassword : async (login,password) =>{
        const success = await connectionWrapper(async()=>{
            const userLookup = await UserModel.find({login: login})
            if(userLookup.length>0){
               return (await userLookup[0].verifyPass(password))
            }else{
            return false
            }
        })
        return success
    },removeUser : async (login) =>{
        const success = await connectionWrapper(async()=>{
            return await UserModel.findOneAndDelete({login: login})
        })
        return success
    },getUserData : async (session) => {
        const userData = await connectionWrapper(async ()=>{
            const userID = await SessionModel.findById(session)
            const userLookup = await UserModel.findById(userID.user)
            if(userLookup){
                return userLookup
            }else{
                return {}
            }
        })
        return userData
    },getExercises : async () =>{
        const exercises = await connectionWrapper(async ()=>{
            return (await ExerciseModel.find())
        })
        return exercises
    },dbRestart: async() =>{
        const exercisesInital = exercises
        const result =  await connectionWrapper(async ()=>{
            const existing = await mongoose.connection.db.listCollections().toArray()
            existing.forEach(async (col)=>{
                if(col.name==='usersessions'){
                    await SessionModel.collection.drop()
                }else if(col.name==='users'){
                    await UserModel.collection.drop()
                }else if(col.name==='exercises'){
                   await ExerciseModel.collection.drop()
                }
            })
            let exerciseKeys = {}
            const exercisesToAdd = []
            exercisesInital.forEach((ex)=>{
                exercisesToAdd.push(new ExerciseModel({name: ex.name,muscles: ex.muscles}))
            })
            const addedExercises = await ExerciseModel.create(exercisesToAdd)
            addedExercises.forEach((ex=>{
                exerciseKeys[ex.name]=ex._id
            }))
            const exampleUser = {
                login: DUMMYLOGIN,
                password: DUMMYPASS,
                workouts: [
                    {
                        name: 'first',
                        created: Date.now(),
                        exercises: [
                            {
                                exercise: exerciseKeys['Landmine'],
                                reps: 10,
                                weight: 5
                            },{
                                exercise: exerciseKeys['Bicycle'],
                                reps: 50,
                                weight: 0
                            },{
                                exercise: exerciseKeys['Flag'],
                                reps: 2,
                                weight: 10
                            },{
                                exercise: exerciseKeys['Rest'],
                                reps: 3,
                                weight: 0
                            },

                        ]
                    }
                ]
            }
            await (new UserModel(exampleUser)).save()
            return true
        })
        return result
    },modifyWorkouts : async(workouts, login) =>{
        let success = false;
        await connectionWrapper(async()=>{
            let userLookup = await UserModel.findOne({login: login})
            userLookup.workouts = workouts
            let savedUser = await userLookup.save()
            if(savedUser){
                success = true
            }
        })
        return success
    },changePassword: async(login,oldPassword,newPassword) =>{
        const success = await connectionWrapper(async()=>{
            const userLookup = await UserModel.find({login: login})
            if(userLookup.length>0){
                const validOldPass = await userLookup[0].verifyPass(oldPassword)
                if(validOldPass){
                    userLookup[0].password = newPassword;
                    const savedUser = await userLookup[0].save()
                    if(savedUser){
                        return true;
                    }
                }
            }
            return false
        })
        return success
    }

}
module.exports = mongoHandlers