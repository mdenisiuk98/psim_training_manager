const express = require('express');
const app = express();
const path = require('path');
const bodyParser = require('body-parser')
const authRoutes = require('./routes/authRoutes')
const contentRoutes = require('./routes/contentRoutes')

app.set('port', process.env.PORT || 3000)

const root = path.join(__dirname, 'client', 'build')

const forceSSL = (req, res, next) => {

    if (process.env.ENV !== 'dev') {
        if (req.headers['x-forwarded-proto'] !== 'https') {
            return res.redirect(302, `https://${req.hostname}${req.originalUrl}`);
        }
        return next();
    }
    else{
        return next();
    }
}



app.use(forceSSL)
app.use(express.static(root))
app.use(bodyParser.json())
app.use('/auth', authRoutes)
app.use('/api', contentRoutes)
app.get('*', (req, res, next) => {
    res.sendFile('index.html', { root });
})

app.listen(app.get('port'), server => {
    console.info(`Server listen on port ${app.get('port')}`);
})