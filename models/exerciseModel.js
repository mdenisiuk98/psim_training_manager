const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator')

const exerciseSchema = new mongoose.Schema({
    name: {type: String, required: true, unique: true},
    muscles: [String],
})
exerciseSchema.plugin(uniqueValidator)
const exerciseModel = mongoose.model('Exercise',exerciseSchema);

module.exports = exerciseModel