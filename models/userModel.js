const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const salt_num = 10;
const uniqueValidator = require('mongoose-unique-validator')
var idvalidator = require('mongoose-id-validator');

const userSchema = new mongoose.Schema({
    login: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    workouts: [{
        name: String,
        created: Date,
        exercises: [{
            exercise: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Exercise'
            },
            reps: Number,
            weight: Number,
        }]
    }]
})
userSchema.plugin(idvalidator)
userSchema.plugin(uniqueValidator)
userSchema.pre('save',function(next){
    var user = this;
    if(!this.isModified('password')){
        return next();
    }
    bcrypt.genSalt(salt_num,async (err,salt)=>{
        if(err) return next(err);

        await bcrypt.hash(user.password,salt,(err,encrypted)=>{
            if(err) return next(err)
            user.password = encrypted;
            next();
        });
    });
});
userSchema.methods.verifyPass = async function (password){
    const tester = await bcrypt.compare(password,this.password)
    return tester
}

module.exports = mongoose.model('User',userSchema)