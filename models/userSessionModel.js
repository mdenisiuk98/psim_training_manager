const mongoose = require('mongoose');
var idvalidator = require('mongoose-id-validator');

const userSessionSchema = new mongoose.Schema({
    valid: {type: Boolean, required: true},
    user: {  
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
})
userSessionSchema.plugin(idvalidator)
const userSessionModel = mongoose.model('UserSession',userSessionSchema);

module.exports = userSessionModel