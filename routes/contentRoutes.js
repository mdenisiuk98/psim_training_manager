const router = require('express').Router();
const { getUserData, getExercises, dbRestart, modifyWorkouts } = require('../middleware/mongoHandlers')
const { NUKEROUTE } = process.env
router.use(function (req, res, next) {
    next()
  })
router.post('/userData/', async (req, res) => {
  try{
    const user = await getUserData(req.body.sessionID)
    res.send({user: {login: user.login,workouts: user.workouts}})
  }catch(err){
    res.send({user: {}})
  }
});
router.post('/modifyWorkouts', async (req, res) =>{
  try{
    const success = await modifyWorkouts(req.body.workouts, req.body.login)
    res.send({success: success})
  }catch(err){
    res.send({success: false})
  }
})

router.get('/exercisesData', async (req, res) => {
  try{
    const exercises = await getExercises()
    res.send({exercises: exercises})
  }catch(err){
    res.send({exercises: []})
  }
});

router.get(`/nuke/:key`, async (req, res) => {
    console.log(`Restarting database...`)
    if(req.params.key===NUKEROUTE){
      const success = await dbRestart()
      if(success){
        console.log('Successfully reset database!')
        res.send({success: true})
      }else{
        res.send({success: false})
      }
    }else{
      res.send({success: false})
    }
});
module.exports=router