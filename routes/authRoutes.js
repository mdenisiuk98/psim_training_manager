const router = require('express').Router();
const { newUser, login, logout, verifySession, verifyPassword, removeUser, changePassword } = require('../middleware/mongoHandlers')
router.use(function (req, res, next) {
     next()
})
router.post('/login', async (req, res) => {
    let success = false;
    let session = await login(req.body.login,req.body.password)
    if(session){
        success=true
    }
    return res.send({success:success,session:session})
})

router.post('/register', async (req, res) => {
    let success=false;
    let toAdd = {
        login: req.body.login,
        password: req.body.password,
        workouts: []
    }
    let addedUser = null
    try{
        addedUser = await newUser(toAdd)
    }catch(err){
        console.log(err)
    }
    if(Object.keys(addedUser).length>0){
        success=true
    }
   return res.send({success:success})	
})

router.post('/logout', async (req, res) => {
    const success = await logout(req.body.session)
    return res.send({success: success})
})

router.post('/verifySession', async (req, res) => {
    const success = await verifySession(req.body.session, req.body.login)
    res.send({success: success})	
})

router.post('/removeAcc',async (req,res)=>{
    let success = false;
    if(await verifySession(req.body.session, req.body.login)&& await verifyPassword(req.body.login,req.body.password)){
        success = await removeUser(req.body.login)
    }
    if(success){
        success=true
    }
    res.send({success: success})
})

router.post('/verifyPassword', async (req, res) => {
    const success = await verifyPassword(req.body.login,req.body.password)
    res.send({success: success})	
})

router.post('/changePassword', async(req,res)=>{
    const success = await changePassword(req.body.login, req.body.oldPassword, req.body.newPassword)
    res.send({success: success})
})

module.exports = router
